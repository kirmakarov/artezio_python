# -*- coding: utf-8 -*-
import argparse
import os
import re
import requests
import threading
from collections import OrderedDict, defaultdict
from datetime import datetime
from lxml import etree


class TorrentTracker:
    """
    Класс предоставляет методы для получения из RSS торрентов, отбора и скачивания торрент-файлов.

    Принимает ссылку на RSS ленту и список категорий, по которым выполняется группировка файлов.
    """
    def __init__(self, url, categories, proxies):
        self.url = url
        self.categories = categories
        self.torrents = defaultdict(list)
        self.proxies = proxies


    def new_torrents(self):
        """
        Получение списка новых торрентов сгруппированных по категориям.

        :return: при успешном выполнении True, при не успешном False.
        """

        # TODO timeout изменить
        # TODO добавить флаг состояния коннекта - использ прокси или нет
        try:
            urlopen = requests.get(self.url, timeout=15)
        except requests.ConnectionError:
            # Попытка получить доступ к ресурсу с использование прокси сервера
            urlopen = requests.get(self.url, timeout=30, proxies=self.proxies)
        if urlopen.status_code != requests.codes.ok:
            print u'Неуспешная попытка получения списка торрентов с ресурса', self.url
            print 'Code:', urlopen.status_code
            return False
        parser = etree.XMLParser(ns_clean=True) # TODO зачем это
        xml = etree.fromstring(urlopen.text.encode('utf-8'), parser) # TODO зачем энкод?

        titles = xml.xpath('//item/title')
        links = xml.xpath('//item/link')
        categories = xml.xpath('//item/category')
        # Обработка случая, когда китегории указаны в другом пространстве имен
        if not categories:
            categories = xml.xpath('//item/nyaa:category', namespaces={'nyaa': 'https://nyaa.si/xmlns/nyaa'})

        # TODO передалать распределение
        # TODO защита от нарушения структуры
        # TODO получать сразу текстовые значения .text()
        # Распределение торрентов по категориям
        for category, name, link in zip(categories, titles, links):
            for group, keywords in self.categories.items():
                match = any((kw for kw in keywords if kw in category.text))
                if match:
                    self.torrents[group].append((name.text, link.text))
                    break
            else:
                self.torrents['other'].append((name.text, link.text))
        for k, v in self.torrents.items():
            for i, n in v:
                print k, ':: ', i, ': ', n
        return True

    def filtered_by_categories(self, download_categories):
        """
        Выполняет отбор файлов по выбранным категориям.

        Изменяет объект self.torrents.
        :param download_categories: список категорий
        :return: None.
        """
        self.torrents = {cat: names for cat, names in self.torrents.items() if cat in download_categories}

    def filtered_by_name(self, filters):
        """
        Выполняет отбор файлов в соответствии с шаблоном.

        Изменяет объект self.torrents.
        :param filters: список шаблонов
        :return: None
        """
        # TODO Слишком ного вложенности. Не более 3х.
        tmp = []
        for torrents in self.torrents.values():
            for torrent, link in torrents:
                for filter in filters:
                    if filter in torrent:
                        # self.torrents_download[torrent] = link
                        tmp.append((torrent, link))
        self.torrents = {'common': tmp}

    def download_file(self, name, link, path, proxies=None):
        """
        Выполняет скачивание файла.

        :param name: имя файла
        :param link: сслка на скачивание
        :param path: путь где сохранить файл
        :param proxies: прокси сервер
        :return: None
        """
        # Замена недопустимых символов в имени файла
        name = re.sub('[\\/:*?"<>|+]', '', name)
        # TODO что если путь на кирилиыце???
        file_path = os.path.join(path, name + '.torrent')
        # Проверка существования файла с таким именем
        if not os.path.exists(file_path):
            try:
                download = requests.get(link, timeout=15)
            except requests.ConnectionError:
                # Попытка скачать файл с использование прокси сервера
                download = requests.get(link, timeout=30, proxies=proxies)
            with open(file_path, 'wb') as new_file:
                new_file.write(download.content)


class Manage:
    def __init__(self):
        # TODO сделанть переменными класса, а не экземпляра
        # Список RSS торент-трекеров
        self.torrent_urls = [
            'https://www.tokyotosho.info/rss.php',
            'https://anidex.info/rss/?cat=',
            'https://nyaa.si/?page=rss'
        ]
        # Список прокси серверов
        self.proxies = {
            'http': 'http://67.205.133.13:80',
            'https': 'http://185.97.252.54:3128',
        }
        # self.proxies = None

        # Директория для загрузки файлов. Задается относительно расположения скрпта
        # TODO если переменная используется однажды - убрать ее!
        download_folder = 'downloads'
        self.download_path = os.path.join(os.getcwd(), download_folder)

        # Ссылка на шаблон загрузки файлов
        filters_file = 'patterns.txt'
        self.filters = {line.decode('utf-8').strip() for line in open(filters_file)}
        # self.filters = []
        # TODO нужно закрывать файл?

        # Список категорий торрентов и их ассоциативных названий
        self.categories = OrderedDict([
            ("anime", ['Anime', 'Raws']),
            ("manga", ['Manga']),
            ("adult_video", ['Adult Video', 'JAV', 'Hentai']),
            ("music", ['Music', 'Audio', u'♫']),
            ("live_action", ['Live Action', 'LA']),
            ("lighs_novel", ['Lighs Novel']),
            ("pictures", ['Pictures']),
            ("applications", ['Applications']),
            ("games", ['Games']),
            ("other", [])
        ])

        # Обработка ключа запуска скрипта
        cmd_parser = argparse.ArgumentParser()
        cmd_parser.add_argument(
            '-c',
            nargs='+',
            dest='categories',
            choices=self.categories.keys(),
            help=u'Перечисление категорий по которым нужно скачать тррент файлы'
        )
        self.download_categories = cmd_parser.parse_args().categories
        # Удаляем категорию 'other' для исключения лишнего прогона при распределение торрентов по категориям
        del self.categories['other']

    def get_new_torrent(self):
        threads = []
        for url in self.torrent_urls:
            tracker = TorrentTracker(url, self.categories, self.proxies)
            try:
                state = tracker.new_torrents()
            except requests.exceptions.ProxyError:
                print u'Не удается подключиться к прокси серверу'
            except requests.exceptions.ConnectionError:
                print u'Не удается подключить доступ к ресурсу ', tracker.url
            else:
                if not state:
                    break
                if self.download_categories:
                    tracker.filtered_by_categories(self.download_categories)
                if self.filters:
                    tracker.filtered_by_name(self.filters)
                for torrents in tracker.torrents.values():
                    for name, link in torrents:
                        # Параллельная загрузка файлов
                        thread = threading.Thread(
                            target=tracker.download_file,
                            args=(name, link, self.download_path, self.proxies)
                        )
                        thread.start()
                        threads.append(thread)
        # Ожидание завершения скачивания всех файлов
        for thread in threads:
            thread.join()
        print 'Загрузка торрент файлов завершена.'


if __name__ == '__main__':
    t1 = datetime.now()

    work = Manage()
    work.get_new_torrent()

    # вычисление времени работы скрипта
    t2 = datetime.now()
    print('=' * 20 + ' time ' + '=' * 20)
    print(t2 - t1)
