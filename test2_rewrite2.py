# -*- coding: utf-8 -*-

"""test2 rewrite2"""
import argparse
import os
import re
import threading
import requests

from collections import OrderedDict, defaultdict
from datetime import datetime
from lxml import etree


class Manage(object):
    """Менеджер получения торрентов"""
    categories = OrderedDict([
        ("anime", ['Anime', 'Raws']),
        ("manga", ['Manga']),
        ("adult_video", ['Adult Video', 'JAV', 'Hentai']),
        ("music", ['Music', 'Audio', u'♫']),
        ("live_action", ['Live Action', 'LA']),
        ("lighs_novel", ['Lighs Novel']),
        ("pictures", ['Pictures']),
        ("applications", ['Applications']),
        ("games", ['Games']),
    ])
    timeouts = (3, 20)

    def __init__(self):
        # Обработка ключей запуска скрипта
        cmd_parser = argparse.ArgumentParser()
        # Список категорий
        cmd_parser.add_argument('-c', nargs='+', dest='categories',
                                choices=list(self.categories) + ['other'],
                                help=u'Перечисление категорий по которым нужно скачать тррент файлы')
        # Список прокси серверов
        cmd_parser.add_argument('-p', nargs=2, dest='proxies', default=[None, None],
                                help=u"""Адрес https прокси-сервера. Формат: 'proto ip:port'.
                                Пример: 'https 185.97.252.54:3128)'""")
        # Директория для загрузки файлов. Задается относительно расположения скрпта
        cmd_parser.add_argument('-dp', nargs=1, dest='download_path', required=True,
                                help=u'Директория для загрузки файлов. (Относительно расположения скрпта.)')
        # Шаблон загрузки файлов
        cmd_parser.add_argument('-f', nargs=1, dest='filters', default=[None],
                                help=u'Имя файла с шаблонами для загрузки файлов. (Относительно расположения скрпта.)')

        self.download_categories = cmd_parser.parse_args().categories
        self.proxies = {cmd_parser.parse_args().proxies[0]: cmd_parser.parse_args().proxies[1]}
        self.download_path = './' + cmd_parser.parse_args().download_path[0]
        if not os.path.isdir(self.download_path):
            os.mkdir(self.download_path)
        self.filters = None
        # self.use_proxy = False
        filters_file = cmd_parser.parse_args().filters[0]
        if filters_file:
            with open(filters_file) as new_file:
                self.filters = {line.decode('utf-8').strip() for line in new_file}

    def download_file(self, name, link):
        """
        Выполняет скачивание файла.

        :param name: имя файла
        :param link: сслка на скачивание
        :return: None
        """
        # Замена недопустимых символов в имени файла
        name = re.sub('[\\/:*?"<>|+]', '', name)
        file_path = os.path.join(self.download_path, name + '.torrent')
        # Проверка существования файла с таким именем
        if not os.path.exists(file_path):
            if not self.use_proxy:
                try:
                    download = requests.get(link, timeout=self.timeouts)
                except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
                    # Попытка скачать файл с использование прокси сервера
                    self.use_proxy = True
                    download = requests.get(link, timeout=self.timeouts, proxies=self.proxies)
            else:
                download = requests.get(link, timeout=self.timeouts, proxies=self.proxies)
            with open(file_path, 'wb') as new_file:
                for chunk in download.iter_content(chunk_size=512):
                    new_file.write(chunk)

    def download_files(self, kw_list):
        """
        Запускает параллельное скачивание файлов.

        :param kw_list: словарь файлов на скачивание (формат - name: link)
        :return: список тредов скачивающих файлы
        """

        threads = []
        # установка флага необходимости использовать прокси-сервер
        self.use_proxy = False
        for torrents in kw_list.values():
            for name, link in torrents:
                # Параллельная загрузка файлов
                thread = threading.Thread(
                    target=self.download_file,
                    args=(name, link)
                )
                thread.start()
                threads.append(thread)
        return threads

    def get_new_torrents(self, tracker):
        """

        :param tracker: объект торрент-трекера
        :return: список тредов скачивающих файлы
        """
        new_torrents = []
        try:
            new_torrents = self.download_files(tracker.new_torrents())
        except requests.exceptions.ProxyError:
            print u'Не удается подключиться к прокси серверу'
        except requests.exceptions.ConnectionError:
            print u'Не удается подклучить доступ к ресурсу ', tracker.url
        return new_torrents

    def trackers(self):
        """
        Поочередное получение торрентов из торрент-треккеров
        :return: None
        """
        threads = []
        trackers = [
            Nyaa(self.categories, self.download_categories, self.proxies, self.filters),
            Tokyotosho(self.categories, self.download_categories, self.proxies, self.filters),
            Anidex(self.categories, self.download_categories, self.proxies, self.filters),

        ]
        for tracker in trackers:
            try:
                threads.extend(self.get_new_torrents(tracker))
            except (requests.exceptions.ConnectionError, ConnectionError):
                print u'Неуспешная попытка получения списка торрентов с ресурса', tracker.url

        # Ожидание завершения скачивания всех файлов
        for thread in threads:
            thread.join()
        print u'Загрузка торрент файлов завершена.'


class TorrentTracker(object):
    """Общий класс для торрент-треккеров"""
    url = None

    def __init__(self, categories, download_categories=None, proxies=None, filters=None):
        self.categories = categories
        self.filters = filters
        self.download_categories = download_categories
        self.proxies = proxies
        self.timeouts = (3, 15)

    def download_rss(self):
        """
        Скачивает rss ленту торрент-трекера
        :return: XML object
        """
        try:
            response = requests.get(self.url, timeout=self.timeouts)
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
            # Попытка получить доступ к ресурсу с использование прокси сервера
            response = requests.get(self.url, timeout=self.timeouts, proxies=self.proxies)

        if response.status_code != requests.codes.ok:
            print 'Code:', response.status_code
            raise ConnectionError()
        return etree.fromstring(response.text.encode('utf-8'))

    def xml_parser(self, xml):
        """
        Разбирает переданный XML. Формирует списки торрентов разбитых по категориям
        :param xml: XML object
        :return: Словарь со списками торрентов разбитых по категориям.
            Формат - '{категория: (имя, ссылка)}'.
        """
        torrents = []
        for i in range(1, int(xml.xpath('count(//item)'))):
            title = xml.xpath('//item[{}]/title/text()'.format(i))[0]
            link = xml.xpath('//item[{}]/link/text()'.format(i))[0]
            category = xml.xpath('//item[{}]/category/text()'.format(i))[0]
            torrents.append((category, title, link))
        # Распределение торрентов по категориям
        return self.group_by_category(torrents)

    def group_by_category(self, torrents_list):
        """
        Группирует список торрентов по единому списку катеогорий
        :param torrents_list: Список из кортежей. Формат - (категория, имя, ссылка)
        :return: Словарь со списками торрентов разбитых по категориям.
            Формат - '{категория: (имя, ссылка)}'.
        """
        torrents = defaultdict(list)
        for category, name, link in torrents_list:
            for group, keywords in self.categories.items():
                match = any((kw for kw in keywords if kw in category))
                if match:
                    torrents[group].append((name, link))
                    break
            else:
                torrents['other'].append((name, link))
        return torrents

    def filtered_by_name(self, gr_torrents):
        """
        Формирует список торрентов удовлетворяющих заданному шаблону.
        :param gr_torrents: Словарь со списками торрентов разбитых по категориям.
        :return: Словарь с ключем 'common' содержащий списк торрентов удовлетворяющих заданному шаблону.
        """
        right = []
        torrents = []
        # torrents = [torrent for tor_list in gr_torrents.values() for torrent in tor_list]
        map(torrents.extend, gr_torrents.values())
        for torrent, link in torrents:
            right.extend([(torrent, link) for filter_elem in self.filters if filter_elem in torrent])
        return {'common': right}

    def new_torrents(self):
        """
        Получение списка новых торрентов по указанным категориям и в соответствии с заданным шаблоном.
        :return: Словарь содержащий список торрентов удовлетворяющих заданному шаблону.
            Формат - '{категория: (имя, ссылка)}'.
        """
        torrents = self.xml_parser(self.download_rss())
        if self.download_categories:
            torrents = {cat: names for cat, names in torrents.items() if cat in self.download_categories}
        if self.filters:
            torrents = self.filtered_by_name(torrents)
        return torrents


class Anidex(TorrentTracker):
    """Треккер Anidex"""
    url = 'https://anidex.info/rss/?cat='


class Tokyotosho(TorrentTracker):
    """Треккер Tokyotosho"""
    url = 'https://www.tokyotosho.info/rss.php'


class Nyaa(TorrentTracker):
    """Треккер Nyaa"""
    url = 'https://nyaa.si/?page=rss'

    def xml_parser(self, xml):
        """
        Разбирает переданный XML. Формирует списки торрентов разбитых по категориям
        :param xml: XML object
        :return: Словарь со списками торрентов разбитых по категориям.
            Формат - '{категория: (имя, ссылка)}'.
        """
        torrents = []
        for i in range(1, int(xml.xpath('count(//item)'))):
            title = xml.xpath('//item[{}]/title/text()'.format(i))[0]
            link = xml.xpath('//item[{}]/link/text()'.format(i))[0]
            category = xml.xpath('//item[{}]/nyaa:category/text()'.format(i),
                                 namespaces={'nyaa': 'https://nyaa.si/xmlns/nyaa'})[0]
            torrents.append((category, title, link))
        # Распределение торрентов по категориям
        return self.group_by_category(torrents)


class ConnectionError(IOError):
    """Ошибка соединения"""
    def __str__(self):
        return u'Неуспешная попытка соединения с ресурсом'


if __name__ == '__main__':
    T1 = datetime.now()
    WORK = Manage()
    WORK.trackers()
    # вычисление времени работы скрипта
    T2 = datetime.now()
    print '=' * 20 + ' time ' + '=' * 20
    print T2 - T1
