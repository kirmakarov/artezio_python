import re
from collections import Counter, defaultdict
import os
from datetime import datetime
from sys import exit
t1 = datetime.now()

# Блок параметров
# путь к файлу
file_path = 'access2.log'

# включить учет визитов на сервер ботов
vizit_scan_full = 1

# 0 - учет успешных запросов, 1 - учет всех запрос на сервер
full_stat = 0

# массив с наименованием платформ
os_platforms = {
    'bot': ['bot', 'validator', 'spider', 'yahoo! slurp', 'alexa.com', 'aport', 'stackrambler'],
    'macos': ['macintosh'],
    'ios': ['ipad', 'ios', 'iphone'],
    'android': ['android'],
    'linux': ["linux", "alpine", "alt", "antergos", "antix", "arch", "centos", "chromium", "cros", "debian", "devuan",
              "elementary", "fedora", "galliumos", "gentoo", "guixsd", "kali", "knoppix", "mint", "mageia", "mandriva",
              "manjaro", "mepis", "parabola", "scientific", "slackware", "solus", "steamos", "suse", "tails",
              "trisquel", "ubuntu", "void", "xandros", "наулинукс"],
    'unix': ['bsd', 'solaris', 'hp-ux', 'hp ux'],
    'win': ['win']
}

clients = defaultdict(int)
# clients = dict()
platforms = defaultdict(int)

# проверка файла по заданному пути
if not os.path.isfile(file_path) or os.path.getsize(file_path) < 2:
    print("По указанному пути файл не найден, либо файл пустой")
    exit(1)

templ_visit_code = re.compile(r'(?:[45]){1}\d\d|30[^4]')
parse_line = re.compile(r' \[|\] \"|\" \"|\"| \"')

urls_not_useful = ['/lib/', 'robots.txt', 'favicon.ico', '/sitemap']

# открытие построчное чтение файла
with open(file_path) as log_file:
    for line in log_file:
        line = line.lower()
        # разделение строки на компоненты и контроль входного формата строки
        try:
            values = parse_line.split(line)
            ip = values[0].strip(' -')
            date = values[1]
            url = values[2]
            cod, send_data = values[3].split()
            user_agent = values[5]
        except IndexError:
            print('except: строка имеет иной формат')
            continue
        # определение севершен ли визит ботом
        if vizit_scan_full == 0:
            it_bot = False
            for bot in os_platforms['bot']:
                if line.find(bot) >= 0:
                    it_bot = True
                    continue
            if it_bot:
                continue

        # отбрасываем сервисные страницы
        service_page = False
        for elem in urls_not_useful:
            if url.find(elem) >= 0:
                service_page = True
                break
        if service_page:
            continue

        # смотрим код ответа сервера, далем вывод было ли посещение (код 304 считается посещением)
        if full_stat == 0 and templ_visit_code.search(cod) is not None:
            # print('detect ERROR', code_answ)
            continue

        # добавляем посещение для данного клиента
        # вариант 1 - дольше чем if else на 0,21%
        clients[ip] += 1

        # вариант 2 - дольше чем if else на 0,37%
        # try:
        #     clients[ip] += 1
        # except:
        #     clients[ip] = 1

        # вариант 3 - дольше чем if else на 0,4%
        # clients[ip] = clients.get(ip, 0) + 1

        # вариант 4 - наиболее быстрый вариант
        # if ip in clients:
        #     clients[ip] += 1
        # else:
        #     clients[ip] = 1

        # определение с какой плтаформы был совершен визит
        platform_find = False
        user_agent = user_agent.replace('darwin', '')
        for platform, names in os_platforms.items():
            for name in names:
                if name in user_agent:
                    platforms[platform] += 1
                    platform_find = True
                    break
            if platform_find:
                break
        else:
            platforms['unknown'] += 1

# составление рейтинга 10 клиентов данного сервера запросивших самое большое количество страниц

clients = Counter(clients)
top_visitors = clients.most_common(10)
print('10 клиентов сервера запросивших самое большое количество страниц')
for cli, visit in top_visitors:
    print('клиет ', cli, ': ', visit, 'визитов (-а)')

platforms = Counter(platforms)
top_platforms = platforms.most_common(5)

# составление рейтинга 5 самых популярных платформ для запуска программ просмотра веб-страниц
print('-' * 50)
print('5 самых популярных платформ просмотра веб-страниц')
for platform, count in top_platforms:
    print(platform, ': ', count)

# вычисление времени работы скрипта
t2 = datetime.now()
print('=' * 20 + ' time ', '=' * 20)
print(t2 - t1)
