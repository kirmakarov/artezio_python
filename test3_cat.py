# -*- coding: utf-8 -*-

"""My version cat - concatenate files and print on the standard output"""

import argparse
import sys


def get_arg_params():
    """Обработка ключей запуска скрипта"""
    cmd_parser = argparse.ArgumentParser(description='Concatenate files and print on the standard output')
    cmd_parser.add_argument(
        nargs='*',
        dest='file_list',
        help='List of files'
    )
    print(cmd_parser.parse_args().file_list)
    return cmd_parser.parse_args().file_list


def get_sys_argv_params():
    """Обработка ключей запуска скрипта"""
    args = [arg for arg in sys.argv if (arg == '-' or (not arg.startswith('-')and len(arg) > 1))]
    # args = sys.argv
    # if args[0] != '-':
    # else:
    print(args)

    return args


def cat(*args):
    """Concatenate files and print on the standard output"""
    print(args)
    if args:
        for arg in args:
            if arg == '-':
                perpetual_output_from_input()
            else:
                output_file(arg)
    else:
        perpetual_output_from_input()


def perpetual_output_from_input():
    """Постоянный вывод в output из input"""
    while True:
        print(input())


def output_file(file_name):
    """Вывод в output из содержимого файла"""
    # [print(line.rstrip()) for line in open(file_arg)]
    with open(file_name) as read_file:
        for line in read_file:
            print(line.rstrip())


if __name__ == '__main__':
    get_sys_argv_params()
    # cat(*get_arg_params())
