# -*- coding: utf-8 -*-

"""test4 part1"""

import argparse
import sqlite3

from collections import defaultdict
from datetime import datetime
from os import path

import requests

from lxml import html


class ServConnectionError(ConnectionError):
    """Ошибка соединения"""
    def __str__(self):
        return 'Неуспешная попытка соединения с ресурсом'


class ServerError(ConnectionError):
    """Ошибка со стороны сервера"""
    def __str__(self):
        return 'На сервере возникла внутренняя ошибка.'


class ArgumentError(Exception):
    """Ошибка в аргументе запуска"""


def get_departure_list():
    url = 'http://www.flybulgarien.dk/en/'
    response = requests.get(url, timeout=(1, 5))
    if response.status_code != requests.codes.ok:
        raise ServConnectionError()
    page = html.fromstring(response.text)
    departure_cities = [city for city in page.xpath('//select[@id="departure-city"]/option/@value') if city]
    return departure_cities


def get_arg_params():
    """Обработка ключей запуска скрипта"""
    cmd_parser = argparse.ArgumentParser(description='вывод информации о полётах fly bulgarien')
    cmd_parser.add_argument('-d', nargs=1, dest='departure', required=True,
                            help='IATA-код аэропорта вылета')
    cmd_parser.add_argument('-a', nargs=1, dest='arrival', required=True,
                            help='IATA-код аэропорта прилета')
    cmd_parser.add_argument('-date_depart', nargs=1, dest='departure_date', required=True,
                            help='Дата вылета. Формат: dd.mm.yyyy')
    cmd_parser.add_argument('-date_return', nargs=1, dest='return_date', default=[None],
                            help='Дата обратного вылета. Формат: dd.mm.yyyy')
    return {
        'departure': cmd_parser.parse_args().departure[0].upper(),
        'arrival': cmd_parser.parse_args().arrival[0].upper(),
        'departure_date': cmd_parser.parse_args().departure_date[0],
        'return_date': cmd_parser.parse_args().return_date[0],
    }


def args_checker(kwargs):
    today = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    return_date = None
    departure = kwargs['departure']
    arrival = kwargs['arrival']
    if departure not in get_departure_list():
        raise ArgumentError('Error IATA code departure')
    if len(arrival) != 3 or not arrival.isalpha():
        raise ArgumentError('Error IATA code arrival')
    departure_date = date_from_str(kwargs['departure_date'], 'departure_date')
    if departure_date < today:
        raise ArgumentError('Date of departure before the current date')
    if kwargs['return_date']:
        return_date = date_from_str(kwargs['return_date'], 'return_date')
        if departure_date > return_date:
            raise ArgumentError('Return date before departure date')
    return {
        'departure': departure,
        'arrival': arrival,
        'departure_date': departure_date,
        'return_date': return_date,
    }


def date_from_str(str_date, name_date=None):
    try:
        return datetime.strptime(str_date, "%d.%m.%Y")
    except (TypeError, ValueError):
        raise ArgumentError('Date format "{}" is not correct'.format(name_date))


def check_arrival(departure, arrival):
    url = 'http://www.flybulgarien.dk/script/getcity/2-{}'.format(departure)
    response = requests.get(url, timeout=(1, 5))
    if response.status_code != requests.codes.ok:
        raise ServConnectionError()
    if arrival not in list(response.json()):
        return False
    return True


def get_timetable(departure, arrival, departure_date, return_date=None, passengers=1):
    url = 'https://apps.penguin.bg/fly/quote3.aspx'
    params = {
        'lang': 'en',
        'depdate': datetime.strftime(departure_date, "%d.%m.%Y"),
        'aptcode1': departure,
        'aptcode2': arrival,
        'paxcount': passengers,
        'infcount': ''
    }
    if return_date:
        params['rt'] = ''
        params['rtdate'] = datetime.strftime(return_date, "%d.%m.%Y")
    else:
        params['ow'] = ''
    response = requests.get(url, params=params, timeout=(3, 15))
    if response.status_code != requests.codes.ok:
        raise ServConnectionError()
    page = html.fromstring(response.text)
    if 'An internal error occurred. Please retry your request.' in page.xpath('//body/div[@class="cnf"]/text()'):
        raise ServerError()
    return page


def timetable_parser(page, departure, arrival, departure_date, return_date):
    timetable = defaultdict(list)
    timetable['departure'] = departure
    timetable['arrival'] = arrival
    for tr_id in page.xpath('//table[@id="flywiz_tblQuotes"]/tr/@id'):
        if 'flywiz_rinf' in tr_id:
            flight = parse_flight_info(tr_id, page, departure_date)
            if flight:
                timetable['Going Out'].append(flight)
        elif return_date and 'flywiz_irinf' in tr_id:
            flight = parse_flight_info(tr_id, page, return_date)
            if flight:
                timetable['Coming Back'].append(flight)
    if not timetable['Going Out']:
        timetable['Going Out'].append({'Price': (0, 'EUR')})
    if return_date and not timetable['Coming Back']:
        timetable['Coming Back'].append({'Price': (0, 'EUR')})
    return timetable


def parse_flight_info(tr_id, page, date):
    date_str = page.xpath('//table[@id="flywiz_tblQuotes"]/tr[@id=$name]/td[2]/text()', name=tr_id)[0]
    option = None
    date_flight = datetime.strptime(date_str, '%a, %d %b %y')
    if date == date_flight:
        time_str = page.xpath('//table[@id="flywiz_tblQuotes"]/tr[@id=$name]/td[3]/text()', name=tr_id)[0]
        depart_time = datetime.strptime(time_str, '%H:%M')
        time_str = page.xpath('//table[@id="flywiz_tblQuotes"]/tr[@id=$name]/td[4]/text()', name=tr_id)[0]
        arrive_time = datetime.strptime(time_str, '%H:%M')
        tag = tr_id.replace('rinf', 'rprc')
        price_str = page.xpath('//table[@id="flywiz_tblQuotes"]/tr[@id=$name]/td[2]/text()', name=tag)[0]
        value, valuta = price_str.replace('Price:  ', '').split(' ')
        price = (float(value), valuta)
        option = {
            'Date': date_flight, 'Depart time': depart_time,
            'Arrive time': arrive_time, 'Price': price, 'Class': 'Economy'
        }
    return option


def timetable_printer(timetable):
    out_way = "{} -> {}".format(timetable['departure'], timetable['arrival'])
    back_way = "{} <- {}".format(timetable['departure'], timetable['arrival'])
    num = 1
    if not timetable.get('Coming Back'):
        for flight in timetable['Going Out']:
            print('Variant #', num)
            print(out_way, ': ', messenger(flight))
            print('-' * 120)
            num += 1
    else:
        for flight in timetable['Going Out']:
            going_out = messenger(flight)
            for flight_back in timetable['Coming Back']:
                coming_back = messenger(flight_back)
                print('Variant #', num)
                print(out_way, ' | ', going_out)
                print(back_way, ' | ', coming_back)
                print(total_coast(flight, flight_back))
                print('-' * 120)
                num += 1


def messenger(flight):
    date_format = '%a, %d %b %Y'
    time_format = '%H:%M'
    try:
        date = datetime.strftime(flight['Date'], date_format)
        depart = datetime.strftime(flight['Depart time'], time_format)
        arrive = datetime.strftime(flight['Arrive time'], time_format)
        price = str(flight['Price'][0]) + ' ' + flight['Price'][1]
        class_fly = flight['Class']
        message = 'Date: {} | Depart time: {} | Arrive time: {} | Class: {} | Coast: {}'.format(date, depart, arrive,
                                                                                                class_fly, price)
    except (TypeError, KeyError):
        message = 'No available flights found.'
    return message


def total_coast(flight, flight_back):
    if flight['Price'][1] == flight_back['Price'][1]:
        price_sum = flight['Price'][0] + flight_back['Price'][0]
        return "Total coast: {} {}".format(price_sum, flight['Price'][1])
    return ''


def flybulgarien_flying(departure, arrival, departure_date, return_date=None):
    if check_arrival(departure, arrival):
        timetable_raw = get_timetable(departure, arrival, departure_date, return_date)
        timetable = timetable_parser(timetable_raw, departure, arrival, departure_date, return_date)
        timetable_printer(timetable)
    else:
        print('Рейсов между указанными аэропортами не найдено.')


def add_timetable_to_db(timetable):
    pass


def transform_timetable_from_server(timetable):
    pass


def get_flight_from_db(departure, arrival, departure_date, return_date=None):
    if not path.isfile('test4.bd'):
        create_db()
        return
    conn = sqlite3.connect('test4.db')
    cursor = conn.cursor()
    sql = "SELECT * FROM flybulgarien WHERE 'DEPART IATA'=? and 'ARRIVE IATA'=?"
    # t = 'CPH'
    # cursor.execute("SELECT * FROM flybulgarien WHERE 'DEPART IATA'=?", t)
    cursor.execute(sql, [departure, arrival])
    result = cursor.fetchall()
    return True


def create_db():
    conn = sqlite3.connect('test4.db')
    cursor = conn.cursor()
    cursor.execute("""CREATE TABLE flybulgarien
    ('Route ID' integer primary key, 'DEPART IATA' text, 'ARRIVE IATA' text, 'FLIGHT SCHEDULE' text)
    """
    )
    conn.commit()
    conn.close()


def transform_timetable_from_db(flight):
    pass


def get_flight():
    kwargs = args_checker(get_arg_params())
    flight = get_flight_from_db(**kwargs)
    if not flight:
        departure, arrival, departure_date, return_date = kwargs
        if check_arrival(departure, arrival):
            timetable_raw = get_timetable(departure, arrival, departure_date, return_date)
            timetable = timetable_parser(timetable_raw, departure, arrival, departure_date, return_date)
            add_timetable_to_db(timetable)
            timetable_printer(timetable)
        else:
            print('Рейсов между указанными аэропортами не найдено.')
    else:
        timetable = transform_timetable_from_db(flight)
        timetable_printer(timetable)

    # request to bd
    #    exist db?
    #    create db
    # if response from db false or empty
    #     request to server
    #     if successful
    #           add response to db
    #           return INFO to user
    #     else
    #           return NO FLIGHT to user
    # else
    #     return INFO to user


def main():
    try:
        flybulgarien_flying(**args_checker(get_arg_params()))
    except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout,
            requests.exceptions.ConnectTimeout, ServConnectionError
            ):
        print('Не удается получить доступ к серверу.')
    except ServerError:
        print('На сервере возникла внутренняя ошибка.')
    except ArgumentError as err:
        print('Ошибка в одном из атрибутов')
        print(err)


if __name__ == '__main__':
    main()
