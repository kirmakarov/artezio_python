# -*- coding: utf-8 -*-
import argparse
import os
import re
import requests
import threading

from collections import OrderedDict, defaultdict
from datetime import datetime
from lxml import etree


class Manage:
    categories = OrderedDict([
        ("anime", ['Anime', 'Raws']),
        ("manga", ['Manga']),
        ("adult_video", ['Adult Video', 'JAV', 'Hentai']),
        ("music", ['Music', 'Audio', u'♫']),
        ("live_action", ['Live Action', 'LA']),
        ("lighs_novel", ['Lighs Novel']),
        ("pictures", ['Pictures']),
        ("applications", ['Applications']),
        ("games", ['Games']),
        ("other", [])
    ])
    torrent_urls = [
        'https://www.tokyotosho.info/rss.php',
        'https://anidex.info/rss/?cat=',
        'https://nyaa.si/?page=rss'
    ]

    def __init__(self):
        self.threads = []
        # Обработка ключа запуска скрипта
        cmd_parser = argparse.ArgumentParser()
        cmd_parser.add_argument('-c', nargs='+', dest='categories',
                                choices=self.categories.keys(),
                                help=u'Перечисление категорий по которым нужно скачать тррент файлы')
        self.download_categories = cmd_parser.parse_args().categories
        del self.categories['other']

        # Список прокси серверов
        self.proxies = {'http': 'http://67.205.133.13:80', 'https': 'http://185.97.252.54:3128'}

        # Директория для загрузки файлов. Задается относительно расположения скрпта
        self.download_path = os.path.join(os.getcwd(), 'downloads')

        # Ссылка на шаблон загрузки файлов
        filters_file = 'patterns.txt'
        self.filters = {line.decode('utf-8').strip() for line in open(filters_file)}
        # self.filters = None

    def download_file(self, name, link):
        # Замена недопустимых символов в имени файла
        name = re.sub('[\\/:*?"<>|+]', '', name)
        # TODO что если путь на кирилице???
        file_path = os.path.join(self.download_path, name + '.torrent')
        # Проверка существования файла с таким именем
        if not os.path.exists(file_path):
            try:
                download = requests.get(link, timeout=15)
            except requests.ConnectionError:
                # Попытка скачать файл с использование прокси сервера
                download = requests.get(link, timeout=30, proxies=self.proxies)
            with open(file_path, 'wb') as new_file:
                new_file.write(download.content)

    def download_files(self, kw_list):
        for torrents in kw_list.values():
            for name, link in torrents:
                # Параллельная загрузка файлов
                thread = threading.Thread(
                    target=self.download_file,
                    args=(name, link)
                )
                thread.start()
                self.threads.append(thread)

    def get_new_torrents(self, url):
        tracker = NewTorrents(url, self.categories, self.download_categories, self.proxies, self.filters)
        try:
            tracker.new_torrents()
        except requests.exceptions.ProxyError:
            print u'Не удается подключиться к прокси серверу'
        except requests.exceptions.ConnectionError:
            print u'Не удается подключить доступ к ресурсу ', tracker.url
        else:
            self.download_files(tracker.torrents)

    def trackers(self):
        for url in self.torrent_urls:
            self.get_new_torrents(url)

        # Ожидание завершения скачивания всех файлов
        for thread in self.threads:
            thread.join()
        print 'Загрузка торрент файлов завершена.'


class NewTorrents:
    def __init__(self, url, categories, download_categories=None, proxies=None, filters=None):
        self.url = url
        self.categories = categories
        self.filters = filters
        self.download_categories = download_categories
        self.torrents = defaultdict(list)
        self.proxies = proxies

    def download_rss(self):
        # TODO timeout изменить
        # TODO добавить флаг состояния коннекта - использ прокси или нет
        try:
            urlopen = requests.get(self.url, timeout=15)
        except requests.ConnectionError:
            # Попытка получить доступ к ресурсу с использование прокси сервера
            urlopen = requests.get(self.url, timeout=30, proxies=self.proxies)
        if urlopen.status_code != requests.codes.ok:
            print u'Неуспешная попытка получения списка торрентов с ресурса', self.url
            print 'Code:', urlopen.status_code
            return False
        parser = etree.XMLParser(ns_clean=True)  # TODO зачем это
        self.xml = etree.fromstring(urlopen.text.encode('utf-8'), parser)  # TODO зачем энкод?

    def group_by_category(self, torrents_list):
        for category, name, link in torrents_list:
            for group, keywords in self.categories.items():
                match = any((kw for kw in keywords if kw in category.text))
                if match:
                    self.torrents[group].append((name.text, link.text))
                    break
            else:
                self.torrents['other'].append((name.text, link.text))

        print '=' * 120
        print 'self.url', self.url
        for k, v in self.torrents.items():
            for i, n in v:
                print k, ':: ', i, ': ', n

    def xml_parser(self):
        titles = self.xml.xpath('//item/title')
        links = self.xml.xpath('//item/link')
        categories = self.xml.xpath('//item/category')
        # Обработка случая, когда китегории указаны в другом пространстве имен
        if not categories:
            categories = self.xml.xpath('//item/nyaa:category', namespaces={'nyaa': 'https://nyaa.si/xmlns/nyaa'})
        # TODO передалать распределение
        # TODO защита от нарушения структуры
        # TODO получать сразу текстовые значения .text()
        # Распределение торрентов по категориям
        self.group_by_category(zip(categories, titles, links))
        return True

    def filtered_by_name(self):
        # TODO Слишком ного вложенности. Не более 3х.
        tmp = []
        for torrents in self.torrents.values():
            for torrent, link in torrents:
                for filter in self.filters:
                    if filter in torrent:
                        tmp.append((torrent, link))
        self.torrents = {'common': tmp}

    def new_torrents(self):
        self.download_rss()
        self.xml_parser()
        if self.download_categories:
            self.torrents = {cat: names for cat, names in self.torrents.items() if cat in self.download_categories}
        if self.filters:
            self.filtered_by_name()


if __name__ == '__main__':
    t1 = datetime.now()
    work = Manage()
    work.trackers()
    # вычисление времени работы скрипта
    t2 = datetime.now()
    print('=' * 20 + ' time ' + '=' * 20)
    print(t2 - t1)
