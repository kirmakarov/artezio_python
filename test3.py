# -*- coding: utf-8 -*-

"""test3"""


def perpetual_generator(val):
    """Вечный генератор, который выдаёт всё время одно значение."""
    while True:
        yield val


def myxrange(*args):
    """Собственная реализация xrange()."""
    count_args = len(args)
    if count_args == 1:
        start, end, step = 0, int(args[0]), 1
    elif count_args == 2:
        start, end, step = int(args[0]), int(args[1]), 1
    elif count_args == 3:
        start, end, step = [int(el) for el in args]
    else:
        raise TypeError('range expected 1 - 3 arguments, got {}'.format(count_args))

    if start > end and step < 0:
        while start > end:
            yield start
            start += step
    elif start < end and step > 0:
        while start < end:
            yield start
            start += step
    elif step == 0:
        raise ValueError('arg 3 must not be zero')


def myzip(*args):
    """Собственная реализация zip()."""
    zip_array = []
    for ind in range(min([len(el) for el in args])):
        zip_array.append(tuple([el[ind] for el in args]))
    return zip_array


def keys_values_reverse(kwarg):
    """Возвращает словарь, в котором ключи со значениями поменяны местами. На вход принимает словарь."""
    if len(kwarg.values()) != len(set(kwarg.values())):
        raise ValueError('Выполнить преобразование невозможно')
    return {val: key for key, val in kwarg.items()}


def square_numbers(num_list):
    """Возвращает список, содержащий список квадратов чисел входного списка."""
    only_nums = all([isinstance(num, (int, float)) for num in num_list])
    if not only_nums:
        raise ValueError('Выполнить преобразование невозможно. Во входном массиве не все аргументы числа.')
    return [num ** 2 for num in num_list]


def half_list_nums(num_list):
    """Возвращает список, содержащий каждый второй элемент входного списка."""
    return num_list[1::2]


def square_even_num_uneven_pozition(num_list):
    """Возвращает список, содержащий квадраты чётных элементов на нечётных позициях чисел входного списка."""
    only_nums = all([isinstance(num, (int, float)) for num in num_list])
    if not only_nums:
        raise ValueError('Выполнить преобразование невозможно. Во входном массиве не все аргументы числа.')
    return [num ** 2 for num in num_list[::2] if num % 2 == 0]


def func_test():
    """Проверка работы функций"""
    kw_stor = {
        'http': 'http://67.205.133.13:80',
        'https': 'http://185.97.252.54:3128',
        3546: 23122,
        434.67: 122,
        423343: 'reer',
        }
    nums = [1234, 21, 23345, 456, 665, 2.3356, 56, 235456.688, 2]
    # arr = ('sd', 'blabla', 23, 2, 2)
    arr = 'qwerty'

    print('perpetual_generator():')
    perpetual = perpetual_generator('aaa')
    for _ in range(7):
        print(next(perpetual))

    print('myxrange():')
    my_iter = myxrange(7, 2, -1)
    try:
        for ind in range(80):
            print(next(my_iter))
    except StopIteration:
        print('Stop iteration')

    for ind in myxrange(170, 186, 4):
        print(ind)

    print('myzip():')
    print(myzip(nums, arr))

    print('keys_values_reverse():')
    print(keys_values_reverse(kw_stor))

    print('square_numbers():')
    print(square_numbers(nums))

    print('half_list_nums():')
    print(half_list_nums(nums))

    print('square_even_num_uneven_pozition():')
    print(square_even_num_uneven_pozition(nums))


if __name__ == '__main__':
    func_test()
